These are algorithm exercises that I do regularly, just to be in shape.

I come from a generation where interviews were based primarily on tech questions,
certification like (do you know X language, Y RDBMS, Z appserver, etc). Industry
has changed dramatically, and big players have moved the focus to a radically
different arena: mathematical thinking, in particular, algorithmic thinking.
At the beginning I had a very hard time trying to solve these questions, given
that I saw the required tools at school a couple of decades back. Not to mention
that my jobs so far, did not give me much practice for those tools.

We could say that I started officially at the beginning of 2017, and since then
I am becoming better at solving these problems. It is a very gradual process,
but I am confident that at some point I will master this area. My final goal is
to compete at the international arena and show myself that, with perseverance,
I can do anything that I want (even if it seemed quite hard at the beginning).

I mostly code in Python2/3 due its brevity and elegance, but switch to Java when
I need a more sophisticated data structure (out of the box). It is pity that
Python standard library lacks many of the nice things you can find under
the popular java.util. All the solutions that I post here are mine, and the few
exceptions will explicitly mention where I took them from.

I think it has more value to present my own creations, that just to mirror what
others did; even if I do not always come with a fast solution. I always check,
however, what are the editorial solutions after I finish my own approach. If
there is no editorial solution, I check what others did ... there are some
really amazing minds out there! Looking forward to become like one of them.

The repo is named after the first site I used (topcoder.com's SRMs), though
today I mostly use leetcode.com. Specially the questions for companies like
G(oogle), A(mazon) and similar. You can jump into that section with link below:

https://gitlab.com/dario.mx/topcoder-srm/tree/master/leetcode

At the beginning of times, I tended to backup all my solutions; even the ones
that did not really work. But nowadays, I tend to prune a bit and just backup
here the ones that I find more interesting.

You will also find that old exercises lack documentation, but I am doing an
effort to explain the rationale for new ones. If you want to read the problem
description, please use the link template below:

https://leetcode.com/problems/[subdir-name]

where [subdir-name] can be replaced by the particular subdirectory you are
looking at (there may be several levels of grouping, but the subdirectory I am
referring to here is the bottom level; the one containing the code).

Long life to algorithm problems ...

